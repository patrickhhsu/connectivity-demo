import asyncio
import aiohttp
import json
import xmltodict
import psycopg2
from psycopg2.extras import Json
from psycopg2.extensions import AsIs

BASE_URL = 'https://experimentation.getsnaptravel.com/interview/'

class Query:
    def __init__(self, body):
        self.body = body
        self.requests = [JSONRequest(self.body), XMLRequest(self.body)]
        self.responses = self.generate_requests()

    def generate_requests(self):
        print("Retrieving data...")
        loop = asyncio.get_event_loop()
        future = asyncio.ensure_future(self.send_requests())
        return loop.run_until_complete(future)

    async def send_requests(self):
        async with aiohttp.ClientSession() as session:
            requests = [asyncio.ensure_future(request.post_request(session)) for request in self.requests]
            responses = await asyncio.gather(*requests)
        return [*map(Response, responses)]

    def overlapped_hotels(self):
        '''Return a list of hotel ids that exist in both responses'''

        hotels_list = [response.hotel_prices() for response in self.responses]
        hotel_ids = [list(hotels.keys()) for hotels in hotels_list]
        return set.intersection(*map(set, hotel_ids))

    def responses_to_save(self):
        '''Return a list of responses to save in the database in the desired format.'''

        responses_to_save = [hotel for hotel in self.responses[0].response['hotels'] if hotel['id'] in self.overlapped_hotels() ]
        for hotel in responses_to_save:
            hotel['prices'] = json.dumps({
                'snaptravel': hotel['price'],
                'retail': self.responses[1].hotel_prices()[hotel['id']]
            })
            del hotel['price']
        return responses_to_save

class JSONRequest:
    def __init__(self, body):
       self.body = body

    async def post_request(self, session):
       url = BASE_URL + 'hotels'
       async with session.post(url, json=self.body) as response:
           return await response.json()

class XMLRequest:
    HEADERS = {'Content-Type': 'application/xml'}

    def __init__(self, body):
        self.body = self.__construct_xml(body)

    async def post_request(self, session):
        url = BASE_URL + 'legacy_hotels'
        async with session.post(url, data=self.body, headers=self.HEADERS) as response:
            return await response.text()

    def __construct_xml(self, body):
        return '''
        <?xml version="1.0" encoding="UTF-8"?>
        <root>
            <checkin>{0}</checkin>
            <checkout>{1}</checkout>
            <city>{2}</city>
            <provider>snaptravel</provider>
        </root>
        '''.format(body['checkin'], body['checkout'], body['city'])

class Response:
    def __init__(self, response):
        self.response = self.__convert_response(response)

    def __convert_response(self, response):
        try:
            return {'hotels': xmltodict.parse(response)['root']['element']}
        except TypeError:
            return response

    def hotel_prices(self):
        return {int(hotel['id']): float(hotel['price']) for hotel in self.response['hotels']}

class Database:
    def __init__(self, query):
        self.query = query
        self.conn = psycopg2.connect(dbname='snaptravel', user='patrickhsu')
        self.cur = self.conn.cursor()
        self.create_table()

    def create_table(self):
        create_statement = '''
            CREATE TABLE IF NOT EXISTS hotels(
                id integer NOT NULL UNIQUE,
                hotel_name varchar,
                num_reviews integer,
                address varchar,
                num_stars integer,
                amenities text[],
                image_url varchar,
                prices jsonb)
        '''
        self.cur.execute(create_statement)

    def save(self):
        '''Form insert query from list of hotel dicts and save.'''

        insert_statement = "INSERT INTO hotels (%s) values %s"
        for hotel in self.query.responses_to_save():
            extract_values = [(k, v) for k, v in hotel.items()]
            columns = ','.join([item[0] for item in extract_values])
            values = tuple([item[1] for item in extract_values])
            query = self.cur.mogrify(insert_statement, ([AsIs(columns)] + [values]))
            update = "ON CONFLICT (id) DO UPDATE SET prices = %s"
            query += update.encode()
            self.cur.execute(query, [hotel['prices']])
        self.conn.commit()
        self.cur.close()
        self.conn.close()

def main():
    print('Hello! Please answer the following questions to use this demo!')
    city = input("Which city do you want to go to? ")
    checkin = input("When do you want to go? (checkin) ")
    checkout = input("When are you returning? (checkout) ")

    api_query = Query({
        "city": city,
        "checkin": checkin,
        "checkout": checkout,
        "provider": 'snaptravel'})

    Database(api_query).save()
    print('Data saved. Thanks for using my demo!')

if __name__ == "__main__":
    main()
